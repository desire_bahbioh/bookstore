class BooksController < ApplicationController
  http_basic_authenticate_with name: "dez", password:"riviera3",
    except: [:index, :show]

  def new
    @page_title = 'Add New Book'
    @book = Book.new
    @Category = Category.new
    @author = Author.new
    @publisher = Publisher.new
  end

  def create
    @book = Book.new(book_params)
     if @book.save
      redirect_to books_path
      flash[:notice] = "new book created!"
    else
      render "new"
    end
  end

  def update
    @book = Book.find(params[:id])
    if @book.update(book_params)
      redirect_to book_path
      flash[:notice] = "book has been updated"
    else
      render "edit"
    end

  end

  def edit
    @book = Book.find(params[:id])
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    flash[:notice] = "book has been Deleted!"
    redirect_to books_path
  end

  def index
    @books = Book.all
    @categories = Category.all
  end

  def show
    @book = Book.find(params[:id])
    @categories = Category.all
  end

  private
      def book_params
        params.require(:book).permit(:title, :category_id, :author_id, :publisher_id, :isbn, :year, :price, :buy, :excerpt, :format, :pages, :coverpath)
      end
end
